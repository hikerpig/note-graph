# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.0.2](https://gitlab.com/hikerpig/note-graph/compare/v0.0.1...v0.0.2) (2020-12-02)


### Features

* add `NoteGraphView::updateCanvasSize(size)` method ([22caa4c](https://gitlab.com/hikerpig/note-graph/commit/22caa4cbc82c360809fa70ebcf29d1d12fa73529))
* rename `options.enableForDrag` -> `options.enableNodeDrag` ([ffbeb7b](https://gitlab.com/hikerpig/note-graph/commit/ffbeb7beb04eea9a452e5fc873096dbacfa6fca9))
* tweak some code to make it possible to run example using umd bundle ([7ca8a85](https://gitlab.com/hikerpig/note-graph/commit/7ca8a85ffa7fdc3d86f7c7b80e8bcf013d0d2eda))

### 0.0.1 (2020-12-02)


### Features

* finish essensial features, good to go ([87d623a](https://gitlab.com/hikerpig/note-graph/commit/87d623a2875b07232baca347657a85556d394ae2))
